import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {en_US, ru_RU, zh_CN, sv_SE} from 'ng-zorro-antd-mobile';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {

  minDate = new Date('2000-1-1');
  maxDate1 = new Date('2005-1-1');
  maxDate2 = new Date('2010-1-1');
  maxDate3 = new Date('2015-1-1');
  maxDate4 = new Date('2020-1-1');
  maxDate5 = new Date('2025-1-1');
  maxDate6 = new Date('2030-1-1');
  maxDate7 = new Date('2035-1-1');
  maxDate8 = new Date('2040-1-1');
  maxDate9 = new Date('2045-1-1');
  maxDate10 = new Date('2050-1-1');
  maxDate11 = new Date('2055-1-1');
  maxDate12 = new Date('2060-1-1');

  titleTemplate = '';

  locale = en_US;
  name1 = '选择';
  name2 = '选择';
  name3 = '选择';
  name4 = '选择';

  nowTimeStamp = Date.now();
  now = new Date(this.nowTimeStamp);
  utcNow = new Date(this.now.getTime() + this.now.getTimezoneOffset() * 60000);

  value = [];
  value1 = new Date(2001, 11, 21);
  value2 = new Date();
  value3 = new Date();
  value4 = this.utcNow;

  flag = true;
  index = 0;

  constructor() {
  }

  ngOnInit() {
  }

  currentDateFormat(date, format: string = 'yyyy-mm-dd HH:MM'): any {
    const pad = (n: number): string => (n < 10 ? `0${n}` : n.toString());
    return format
      .replace('yyyy', date.getFullYear())
      .replace('mm', pad(date.getMonth() + 1))
      .replace('dd', pad(date.getDate()))
      .replace('HH', pad(date.getHours()))
      .replace('MM', pad(date.getMinutes()))
      .replace('ss', pad(date.getSeconds()));
  }

  onOk1(result: Date) {
    this.name1 = this.currentDateFormat(result);
    this.value1 = result;
  }

  onOk2(result) {
    this.name2 = this.currentDateFormat(result, 'yyyy-mm-dd');
    this.value2 = result;
  }

  onOk3(result) {
    this.name3 = this.formatIt(result, 'HH:mm');
    this.value3 = result;
  }

  onOk4(result) {
    this.name4 = this.formatIt(result, 'HH:mm');
    this.value4 = result;
  }

  onValueChange(result) {
    this.name1 = this.currentDateFormat(result);
    this.value1 = result;
  }

  formatIt(date: Date, form: string) {
    const pad = (n: number) => (n < 10 ? `0${n}` : n);
    const dateStr = `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`;
    const timeStr = `${pad(date.getHours())}:${pad(date.getMinutes())}`;
    if (form === 'YYYY-MM-DD') {
      return dateStr;
    }
    if (form === 'HH:mm') {
      return timeStr;
    }
    return `${dateStr} ${timeStr}`;
  }

  onChange(item) {
    console.log('onChange', item);
  }

  onTabClick(item) {
    console.log('onTabClick', item);
  }

  selectCard(e) {
    console.log(' ', JSON.stringify(e));
  }

  changeIndex() {
    this.index = 0;
  }
}
